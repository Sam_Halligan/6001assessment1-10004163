**Comp6001 Assessment 1
10004163
Sam Halligan

**Keep things simple and consistent**: 
This is a good gui practice as �the hallmark of a good interface is simplicity� the less explanation the user needs to understand how to use the interface the better job you have done. Keeping consistent sizing and style is also key and nothing should change dramatically throughout the design �Stylistic choices shouldn't break the immersion.�  

**Visual Hierarchy**: 
Implementing a visual hierarchy helps show the user what parts of the site interface have the most importance by making the key parts of your design larger it shows that they are the most important, during this project because it was a simple interface I kept a single size across the board with all my features while still keeping them somewhat large taking up approximately 25% of the sites surface space.

**Single column layout**: 
By having the whole site contained within a single central column it draws attention to it as there is just empty space on either side which does not draw the eye of the user. Many popular websites utilise this style such as Facebook and Twitter with very few things being on the sides of the screen and the bulk of the content being in a single central column.

**Proper use of colour**: 
By correctly using colours you can convey the visual hierarchy of your design for example in my design the first thing which draws your attention is the bright RGB sliders hence showing they are the most important part of the design underneath is the next part which is the buttons for submitting your RGB change and the reset button which are in a lighter shade so they are not as impactful when you first open the site. 

**Presenting all features**:
Not hiding any of the features behind things such as dropboxes is a proven technique which makes the site seem more simple and the user feel more comfortable, therefore presenting everything in one place to the user is much more optimal than hiding things that they need to search for. 

**Short titles**:
On identifiers and buttons use single word descriptions where possible that clearly describe the function. 




**References**: 

Point 1: https://www.elegantthemes.com/blog/resources/10-rules-of-good-ui-design-to-follow-on-every-web-design-project 

Point 2: https://www.elegantthemes.com/blog/resources/10-rules-of-good-ui-design-to-follow-on-every-web-design-project 

Point 3: http://www.goodui.org/

Point 4: https://www.elegantthemes.com/blog/resources/10-rules-of-good-ui-design-to-follow-on-every-web-design-project 

Point 5: http://www.goodui.org/

Point 6: https://stackoverflow.com/questions/90813/best-practices-principles-for-gui-design 
