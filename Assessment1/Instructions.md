**Instructions**

1: Go to the Webpage localhost:5000.
2: Adjust scroll bar or change numbers manually to adjust rgb settings the Preview section near top of page will change to what the colour will come out as.
3: Press the submit button and the whole page will change to that colour.
4: Press the reset button to revert the background to white.
5: Repeat steps 2-4 as many times as you would like.